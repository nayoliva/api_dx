Este projeto visa disponibilizar uma API que faz o parser de uma página de produto da Deal Express (dx.com) utilizando Python (Pycharm), o microframework Flask e utilizando uma técnica chamada web scraping para obter os dados através da biblioteca BeautifulSoup.
Esta API consiste de dois métodos get no endpoint /product-parser e /related-products, ambas esperam como parâmetro a url a qual será realizada um parsing.
O primeiro método /product-parser retornará as seguintes informações no formado json do produto (principal ) da página:
- Título/Nome do produto
 - Descrição do produto
 - Categoria do produto
 - Imagem
 - Preço

O segundo método /related-products trás os mesmos dados de cada produto relacionado ao produto principal em uma lista.

