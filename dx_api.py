#  Copyright 2018 Nayara Oliva Ferreira <naye.ferreira@gmail.com>
from bs4 import BeautifulSoup as bs
import requests
import json


def retorna_json(url):
    dx_logistics = url
    page = requests.get(dx_logistics)
    soup = bs(page.content, 'html.parser')

    wrapper = soup.find(class_="pdetail_wrapper")
    nome = wrapper.find(id="headline").get_text()
    descricao = wrapper.find(class_="short_tit").get_text()
    descricao = descricao.replace('\n', '').replace('\r', '')
    descricao = descricao.strip()
    categoria = soup.find(class_="last").get_text()
    for h in soup.find_all(class_="photo_wrapper"):
        a = h.find('a')
        url = (a.attrs['href'])
    url_completa = "http:" + url
    moeda = wrapper.find(class_="cur_cy").get_text()
    preco = wrapper.find(id="price").get_text()
    valor_preco = moeda + preco

    my_dict = {'name': nome, 'description': descricao, 'category': categoria,
               'image': url_completa, 'price': valor_preco}
    print(my_dict)

    json_data = json.dumps(my_dict)
    return json_data

def retorna_produtos_relacionados(link):
    dx_related_products = link
    r = requests.get(dx_related_products)
    b_soup = bs(r.content, 'html.parser')

    all_product = b_soup.find('div', class_="relate-product")

    dic = {}
    count = 0
    for item in all_product.find_all('ul', class_="productList"):
        for i in item.find_all('li', class_="c_cates"):
            url_truncated = i.a.get('href')
            url_complete = 'http://www.dx.com' + url_truncated
            count = count + 1
            prod_num = 'prod_' + str(count) # key para encontrar todos os dados do produto no teste
            # é preciso fazer um json.load dos dados para que o dicionário não fique entre aspas
            # simples ({'prod_17': '{"category": "Blocks & Jigsaw Toys", ...) permitindo fácil navegação dos dados
            dic_prod = json.loads(retorna_json(url_complete))
            dic[prod_num] = dic_prod

    json_final = json.dumps(dic)
    print(dic)

    return json_final