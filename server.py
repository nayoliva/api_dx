#  Copyright 2018 Nayara Oliva Ferreira <naye.ferreira@gmail.com>
from flask import Flask, request #importa classes
from dx_api import retorna_json, retorna_produtos_relacionados

#criando objeto do tipo Flask para representar a aplicação
app = Flask(__name__)


""" Tente acessar : http://127.0.0.1:8000/product-parser?url=http://www.dx.com/p/diy-3d-puzzle-assembled-model-of-the-millennium-falcon-toy-silver-435383#.WleLKnWnGbk
A função espera um parâmentro, uma url."""
@app.route('/product-parser/', methods=['GET'])
def product_parser():
    url = request.args.get('url')
    return retorna_json(url)

""" Tente acessar : http://127.0.0.1:8000/related-products?url=http://www.dx.com/p/diy-3d-puzzle-assembled-model-of-the-millennium-falcon-toy-silver-435383#.WleLKnWnGbk
A função espera um parâmentro, uma url."""
@app.route('/related-products/', methods=['GET'])
def related_products():
    link = request.args.get('url')
    resp = retorna_produtos_relacionados(link)
    return resp

#roda aplicação
app.run(host='0.0.0.0', port=8000)