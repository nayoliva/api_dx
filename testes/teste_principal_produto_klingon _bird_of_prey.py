#  Copyright 2018 Nayara Oliva Ferreira <naye.ferreira@gmail.com>
# Red Alert! All hands to battle stations! Trekkie coming!
import unittest
from dx_api import retorna_json
import json

#saída esperada
expected_name = 'Klingon Bird-of-Prey Numbers Assembled Model DIY 3D Puzzle Toy'
expected_description = 'DIY Stereo Puzzle, 3D Assembling Star Trek Klingon Bird-of-Prey Jigsaw Puzzle'
expected_category = 'Blocks & Jigsaw Toys'
expected_image = 'http://img.dxcdn.com/productimages/sku_481321_1.jpg'
expected_price = 'R$13.70'

#resultado em formato Json da API
data = retorna_json('http://www.dx.com/p/angry-birds-of-prey-numbers-assembled-model-diy-3d-puzzle-toy-silver-481321#.WnMygXXwY8o')
#decodificação do Json
json_data = json.loads(data)

# testes individuais para cada key
class TestAPI(unittest.TestCase):
    def teste_nome(self):
        result_name = json_data['name']
        self.assertEqual(expected_name, result_name)

    def teste_descricao(self):
        result_description = json_data['description']
        self.assertEqual(expected_description, result_description)

    def teste_categoria(self):
        result_category = json_data['category']
        self.assertEqual(expected_category, result_category)

    def teste_imagem(self):
        result_image = json_data['image']
        self.assertEqual(expected_image, result_image)

    def teste_price(self):
        result_price = json_data['price']
        self.assertEqual(expected_price, result_price)


if __name__ == '__main__':
    unittest.main()
