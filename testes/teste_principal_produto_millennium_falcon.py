#  Copyright 2018 Nayara Oliva Ferreira <naye.ferreira@gmail.com>

import unittest
from dx_api import retorna_json
import json

#saída esperada
expected_name = 'DIY 3D Puzzle Assembled Model Of The Millennium Falcon Toy - Silver'
expected_description = 'DIY Three-Dimensional Jigsaw Puzzle Assembled Metal Millennium Falcon Model Educational Toy'
expected_category = 'Blocks & Jigsaw Toys'
expected_image = 'http://img.dxcdn.com/productimages/sku_435383_1.jpg'
expected_price = 'R$10.95'

#resultado em formato Json da API
data = retorna_json('http://www.dx.com/p/diy-3d-puzzle-assembled-model-of-the-millennium-falcon-toy-silver-435383#.WleLKnWnGbk')
#decodificação do Json
json_data = json.loads(data)

# testes individuais para cada key
class TestAPI(unittest.TestCase):
    def teste_nome(self):
        result_name = json_data['name']
        self.assertEqual(expected_name, result_name)

    def teste_descricao(self):
        result_description = json_data['description']
        self.assertEqual(expected_description, result_description)

    def teste_categoria(self):
        result_category = json_data['category']
        self.assertEqual(expected_category, result_category)

    def teste_imagem(self):
        result_image = json_data['image']
        self.assertEqual(expected_image, result_image)

    def teste_price(self):
        result_price = json_data['price']
        self.assertEqual(expected_price, result_price)


if __name__ == '__main__':
    unittest.main()
