#  Copyright 2018 Nayara Oliva Ferreira <naye.ferreira@gmail.com>
#  Atenção com os dados dos preços, estes sempre mudam no site fazendo os testes falharem
import unittest
from dx_api import retorna_produtos_relacionados
import json

#saída esperada
# PRODUTO 1
expected_name_1 = 'DIY 3D Puzzle Model Assembled Educational Toy Metal  - Silver'
expected_description_1 = 'DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Stainless Steel Robot  Model Educational Toy'
expected_category_1 = 'Blocks & Jigsaw Toys'
expected_image_1 = 'http://img.dxcdn.com/productimages/sku_444790_1.jpg'
expected_price_1 = 'R$11.20'

# PRODUTO 2
expected_name_2 = 'DIY 3D Puzzle Stainless Steel Assembled Lunar Model Toy - Silver'
expected_description_2 = 'DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Model Educational Toy Lunar Lander'
expected_category_2 = 'Blocks & Jigsaw Toys'
expected_image_2 = 'http://img.dxcdn.com/productimages/sku_442067_1.jpg'
expected_price_2 = 'R$11.91'

# PRODUTO 3
expected_name_3 = 'DIY 3D Puzzle Toy Model Assembled Metal ATST - Silver'
expected_description_3 = 'Three-Dimensional Jigsaw Puzzle Assembled Model Stainless Steel ATST Educational Children Toy'
expected_category_3 = 'Blocks & Jigsaw Toys'
expected_image_3 = 'http://img.dxcdn.com/productimages/sku_434603_1.jpg'
expected_price_3 = 'R$11.47'

# PRODUTO 4
expected_name_4 = 'DIY 3D Puzzle AT-AT Troop Assembled Model Educational Toy - Silver'
expected_description_4 = 'Three-Dimensional Assembling All Terrain Armored Transport Walker Model Puzzle Toy'
expected_category_4 = 'Blocks & Jigsaw Toys'
expected_image_4 = 'http://img.dxcdn.com/productimages/sku_435380_1.jpg'
expected_price_4 = 'R$11.20'

# PRODUTO 5
expected_name_5 = 'DIY 3D Puzzle Assembled Model Toy Weapon Fighter - Silver'
expected_description_5 = 'Educational Stainless Steel Three-Dimensional Jigsaw Puzzle Assembling Model Toy'
expected_category_5 = 'Blocks & Jigsaw Toys'
expected_image_5 = 'http://img.dxcdn.com/productimages/sku_437914_1.jpg'
expected_price_5 = 'R$11.20'

# PRODUTO 6
expected_name_6 = 'DIY Puzzle 3D Assembled Bat Car Model Puzzle Toy - Silver'
expected_description_6 = 'DIY Three-Dimensional Stainless Steel Jigsaw 3D Assembly Simulation Bat Car Model Educational Toy'
expected_category_6 = 'Blocks & Jigsaw Toys'
expected_image_6 = 'http://img.dxcdn.com/productimages/sku_453620_1.jpg'
expected_price_6 = 'R$15.90'

# PRODUTO 7
expected_name_7 = 'Metal DIY 3D Puzzle Toy Model Assembled Cessna 172 - Silver'
expected_description_7 = 'Metal DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Model Toy Cessna 172'
expected_category_7 = 'Blocks & Jigsaw Toys'
expected_image_7 = 'http://img.dxcdn.com/productimages/sku_445501_1.jpg'
expected_price_7 = 'R$15.23'

# PRODUTO 8
expected_name_8 = 'DIY 3D Puzzle Assembled Space Shuttle Model Educational Toy - Silver'
expected_description_8 = 'Three-Dimensional Jigsaw Puzzle Assembled Model Stainless Lmperial Shuttle Educational Toy'
expected_category_8 = 'Blocks & Jigsaw Toys'
expected_image_8 = 'http://img.dxcdn.com/productimages/sku_436334_1.jpg'
expected_price_8 = 'R$14.53'

# PRODUTO 9
expected_name_9 = 'DIY 3D Puzzle Assembled Model Toy Enterprise - Silver'
expected_description_9 = 'DIY 3D Educational Jigsaw Puzzle Assembling Stainless Steel Model Toy'
expected_category_9 = 'Blocks & Jigsaw Toys'
expected_image_9 = 'http://img.dxcdn.com/productimages/sku_438401_1.jpg'
expected_price_9 = 'R$12.48'

# PRODUTO 10
expected_name_10 = 'DIY 3D Puzzle Assembled Model Imperial Star Destroyer Toy - Silver'
expected_description_10 = 'DIY 3D Puzzle Assembling Stainless Steel Imperial Star Destroyer Model Educational Toy'
expected_category_10 = 'Blocks & Jigsaw Toys'
expected_image_10 = 'http://img.dxcdn.com/productimages/sku_435370_1.jpg'
expected_price_10 = 'R$11.20'

# PRODUTO 11
expected_name_11 = 'DIY Puzzle 3D Assembled Batmobil Model Car Educational Toy - Silver'
expected_description_11 = 'DIY Stainless Steel Jigsaw 3D Assembled Justice Dawn Batman Wars Batmobil Model Car Educational Toy'
expected_category_11 = 'Blocks & Jigsaw Toys'
expected_image_11 = 'http://img.dxcdn.com/productimages/sku_453621_1.jpg'
expected_price_11 = 'R$15.90'

# PRODUTO 12
expected_name_12 = 'DIY Jigsaw Puzzle, 3D Stainless Steel Metal Planet War Imperial Warships Assembled Model Educational Toy - Silver'
expected_description_12 = 'DIY Three-Dimensional Jigsaw Puzzle, 3D Star Wars Empire Warships Assembled Model Toy for Kids'
expected_category_12 = 'Blocks & Jigsaw Toys'
expected_image_12 = 'http://img.dxcdn.com/productimages/sku_487374_1.jpg'
expected_price_12 = 'R$13.28'

# PRODUTO 13
expected_name_13 = 'DIY Puzzle 3D Assembled Shipship Destroyer Model Toy - Silver'
expected_description_13 = 'DIY 3D Metal Jigsaw Flying Ship Destroyer Simulation Model Educational Toy for Kids / Adults'
expected_category_13 = 'Blocks & Jigsaw Toys'
expected_image_13 = 'http://img.dxcdn.com/productimages/sku_451444_1.jpg'
expected_price_13 = 'R$15.83'

# PRODUTO 14
expected_name_14 = 'DIY 3D Puzzle Flagship Model Assembled Educational Toy - Silver'
expected_description_14 = 'DIY 3D Puzzle Model Assembling Stainless Kai Luolun Command Shuttle Educational Toy'
expected_category_14 = 'Blocks & Jigsaw Toys'
expected_image_14 = 'http://img.dxcdn.com/productimages/sku_435620_1.jpg'
expected_price_14 = 'R$14.73'

# PRODUTO 15
expected_name_15 = 'Star Wars 3D Puzzle, DIY Stainless Steel Metal Flying Robot Assembled Model Educational Toy - Silver'
expected_description_15 = 'DIY Three-Dimensional Puzzle, 3D Star Wars Flying Robot Assembly Model Toy for Kids, Home Decoration'
expected_category_15 = 'Blocks & Jigsaw Toys'
expected_image_15 = 'http://img.dxcdn.com/productimages/sku_488524_1.jpg'
expected_price_15 = 'R$14.19'

# PRODUTO 16
expected_name_16 = 'DIY 3D Puzzle Nano Dutch Windmill Assembled Model Toy - Silver'
expected_description_16 = 'Free Plastic DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Nano-Dutch Windmill Model Toy'
expected_category_16 = 'Blocks & Jigsaw Toys'
expected_image_16 = 'http://img.dxcdn.com/productimages/sku_444899_1.jpg'
expected_price_16 = 'R$14.59'

# PRODUTO 17
expected_name_17 = 'DIY 3D Puzzle Assembled Model Toy Car Mars Rover - Silver'
expected_description_17 = 'DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Mars Exploration Rover Model Educational Toy'
expected_category_17 = 'Blocks & Jigsaw Toys'
expected_image_17 = 'http://img.dxcdn.com/productimages/sku_440381_1.jpg'
expected_price_17 = 'R$15.43'

# PRODUTO 18
expected_name_18 = 'DIY 3D Puzzle Assembled Model Toy Hubble Space Telescope - Silver'
expected_description_18 = 'Stainless Steel DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Model Toy Hubble Space Telescope'
expected_category_18 = 'Blocks & Jigsaw Toys'
expected_image_18 = 'http://img.dxcdn.com/productimages/sku_445371_1.jpg'
expected_price_18 = 'R$17.98'

# PRODUTO 19
expected_name_19 = 'DIY 3D Puzzle Metal Snow Coaster Model Assembled Toy - Silver'
expected_description_19 = 'Three-Dimensional Jigsaw Assembled Stainless Steel Snow Coaster Model Educational Children Toys'
expected_category_19 = 'Blocks & Jigsaw Toys'
expected_image_19 = 'http://img.dxcdn.com/productimages/sku_434606_1.jpg'
expected_price_19 = 'R$14.09'

# PRODUTO 20
expected_name_20 = 'DIY 3D Puzzle Assembled Model Pirate Ship Puzzle Toy - Silver'
expected_description_20 = 'DIY 3D Puzzle Stainless Steel Pirate Ship Assembling Model Educational Toy'
expected_category_20 = 'Blocks & Jigsaw Toys'
expected_image_20 = 'http://img.dxcdn.com/productimages/sku_435374_1.jpg'
expected_price_20 = 'R$16.34'

# PRODUTO 21
expected_name_21 = 'DIY 3D Puzzle Assembled Model Toy Boxes People - Silver'
expected_description_21 = 'Free Plastic DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Model Toy Boxes People'
expected_category_21 = 'Blocks & Jigsaw Toys'
expected_image_21 = 'http://img.dxcdn.com/productimages/sku_445940_1.jpg'
expected_price_21 = 'R$14.19'

# PRODUTO 22
expected_name_22 = 'DIY 3D Puzzle Assembled Model Toy Sailboat Hotel - Silver'
expected_description_22 = 'Free Plastic DIY Stereoscopic 3D Puzzle Nano Assembled Model Toy Sailboat Hotel'
expected_category_22 = 'Blocks & Jigsaw Toys'
expected_image_22 = 'http://img.dxcdn.com/productimages/sku_444967_1.jpg'
expected_price_22 = 'R$13.15'

# PRODUTO 23
expected_name_23 = 'DIY 3D Golden Gate Bridge Style Puzzle Assembled Model Toy - Silver'
expected_description_23 = 'Free Plastic DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Model Toy Golden Gate Bridge'
expected_category_23 = 'Blocks & Jigsaw Toys'
expected_image_23 = 'http://img.dxcdn.com/productimages/sku_445499_1.jpg'
expected_price_23 = 'R$12.95'

# PRODUTO 24
expected_name_24 = 'DIY 3D Puzzle Model Assembled Educational Toy Destroyer - Silver'
expected_description_24 = 'Three-Dimensional Jigsaw Puzzle Assembled Model Stainless Steel Destroyer Educational Toy'
expected_category_24 = 'Blocks & Jigsaw Toys'
expected_image_24 = 'http://img.dxcdn.com/productimages/sku_436336_1.jpg'
expected_price_24 = 'R$14.22'

# PRODUTO 25
expected_name_25 = 'DIY 3D Puzzle Assembled Model Toy Angry Birds Of Prey Numbers - Silver'
expected_description_25 = 'Educational Stainless Steel Three-Dimensional Jigsaw Assembling Model Toy'
expected_category_25 = 'Blocks & Jigsaw Toys'
expected_image_25 = 'http://img.dxcdn.com/productimages/sku_437987_1.jpg'
expected_price_25 = 'R$13.38'

# PRODUTO 26
expected_name_26 = "DIY 3D Puzzle Assembled Model Toy Klingon Vor'cha - Silver"
expected_description_26 = 'Educational Stainless Steel Three-Dimensional Jigsaw Assembling Model Toy'
expected_category_26 = 'Blocks & Jigsaw Toys'
expected_image_26 = 'http://img.dxcdn.com/productimages/sku_437989_1.jpg'
expected_price_26 = 'R$13.38'

# PRODUTO 27
expected_name_27 = "DIY 3D Puzzle Assembled Model Toy Helmet - Silver"
expected_description_27 = 'DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Model Educational Toys Master Chief Helmet'
expected_category_27 = 'Blocks & Jigsaw Toys'
expected_image_27 = 'http://img.dxcdn.com/productimages/sku_439685_1.jpg'
expected_price_27 = 'R$12.95'

# PRODUTO 28
expected_name_28 = "DIY Puzzle, 3D Stainless Steel Metal Space Human Head Assembled Model Toy, Creative Gift - Silver"
expected_description_28 = 'DIY Three-Dimensional Puzzle, 3D Creative Space Head Assembled Model Toy for Kids'
expected_category_28 = 'Blocks & Jigsaw Toys'
expected_image_28 = 'http://img.dxcdn.com/productimages/sku_488503_1.jpg'
expected_price_28 = 'R$14.19'

# PRODUTO 29
expected_name_29 = "DIY 3D Blankenburg Door Style Puzzle Assembled Model Toy - Silver"
expected_description_29 = 'Free Plastic DIY 3D Three-Dimensional Jigsaw Puzzle Assembled Model Toy Blankenburg Door'
expected_category_29 = 'Blocks & Jigsaw Toys'
expected_image_29 = 'http://img.dxcdn.com/productimages/sku_445500_1.jpg'
expected_price_29 = 'R$15.97'

# PRODUTO 30
expected_name_30 = " DIY Puzzle 3D Biplane Model Assembled Educational Toy - Silver"
expected_description_30 = 'Stainless Steel Stereoscopic 3D Puzzle Nano Biplane Model Assembled Educational Toy'
expected_category_30 = 'Blocks & Jigsaw Toys'
expected_image_30 = 'http://img.dxcdn.com/productimages/sku_445220_1.jpg'
expected_price_30 = 'R$13.08'

#resultado em formato Json da API (lista de produtos relacionados ao produto deste link)
data = retorna_produtos_relacionados('http://www.dx.com/p/diy-3d-puzzle-assembled-model-of-the-millennium-falcon-toy-silver-435383#.WleLKnWnGbk')

json_data = json.loads(data)


# testes individuais para cada key
class TestAPI(unittest.TestCase):
    def teste_nome(self):
        result_name_1 = json_data['prod_1']['name']
        result_name_2 = json_data['prod_2']['name']
        result_name_3 = json_data['prod_3']['name']
        result_name_4 = json_data['prod_4']['name']
        result_name_5 = json_data['prod_5']['name']
        result_name_6 = json_data['prod_6']['name']
        result_name_7 = json_data['prod_7']['name']
        result_name_8 = json_data['prod_8']['name']
        result_name_9 = json_data['prod_9']['name']
        result_name_10 = json_data['prod_10']['name']
        result_name_11 = json_data['prod_11']['name']
        result_name_12 = json_data['prod_12']['name']
        result_name_13 = json_data['prod_13']['name']
        result_name_14 = json_data['prod_14']['name']
        result_name_15 = json_data['prod_15']['name']
        result_name_16 = json_data['prod_16']['name']
        result_name_17 = json_data['prod_17']['name']
        result_name_18 = json_data['prod_18']['name']
        result_name_19 = json_data['prod_19']['name']
        result_name_20 = json_data['prod_20']['name']
        result_name_21 = json_data['prod_21']['name']
        result_name_22 = json_data['prod_22']['name']
        result_name_23 = json_data['prod_23']['name']
        result_name_24 = json_data['prod_24']['name']
        result_name_25 = json_data['prod_25']['name']
        result_name_26 = json_data['prod_26']['name']
        result_name_27 = json_data['prod_27']['name']
        result_name_28 = json_data['prod_28']['name']
        result_name_29 = json_data['prod_29']['name']
        result_name_30 = json_data['prod_30']['name']
        self.assertEqual(expected_name_1, result_name_1)
        self.assertEqual(expected_name_2, result_name_2)
        self.assertEqual(expected_name_3, result_name_3)
        self.assertEqual(expected_name_4, result_name_4)
        self.assertEqual(expected_name_5, result_name_5)
        self.assertEqual(expected_name_6, result_name_6)
        self.assertEqual(expected_name_7, result_name_7)
        self.assertEqual(expected_name_8, result_name_8)
        self.assertEqual(expected_name_9, result_name_9)
        self.assertEqual(expected_name_10, result_name_10)
        self.assertEqual(expected_name_11, result_name_11)
        self.assertEqual(expected_name_12, result_name_12)
        self.assertEqual(expected_name_13, result_name_13)
        self.assertEqual(expected_name_14, result_name_14)
        self.assertEqual(expected_name_15, result_name_15)
        self.assertEqual(expected_name_16, result_name_16)
        self.assertEqual(expected_name_17, result_name_17)
        self.assertEqual(expected_name_18, result_name_18)
        self.assertEqual(expected_name_19, result_name_19)
        self.assertEqual(expected_name_20, result_name_20)
        self.assertEqual(expected_name_21, result_name_21)
        self.assertEqual(expected_name_22, result_name_22)
        self.assertEqual(expected_name_23, result_name_23)
        self.assertEqual(expected_name_24, result_name_24)
        self.assertEqual(expected_name_25, result_name_25)
        self.assertEqual(expected_name_26, result_name_26)
        self.assertEqual(expected_name_27, result_name_27)
        self.assertEqual(expected_name_28, result_name_28)
        self.assertEqual(expected_name_29, result_name_29)
        self.assertEqual(expected_name_30, result_name_30)


    def teste_descricao(self):
        result_description_1 = json_data['prod_1']['description']
        result_description_2 = json_data['prod_2']['description']
        result_description_3 = json_data['prod_3']['description']
        result_description_4 = json_data['prod_4']['description']
        result_description_5 = json_data['prod_5']['description']
        result_description_6 = json_data['prod_6']['description']
        result_description_7 = json_data['prod_7']['description']
        result_description_8 = json_data['prod_8']['description']
        result_description_9 = json_data['prod_9']['description']
        result_description_10 = json_data['prod_10']['description']
        result_description_11 = json_data['prod_11']['description']
        result_description_12 = json_data['prod_12']['description']
        result_description_13 = json_data['prod_13']['description']
        result_description_14 = json_data['prod_14']['description']
        result_description_15 = json_data['prod_15']['description']
        result_description_16 = json_data['prod_16']['description']
        result_description_17 = json_data['prod_17']['description']
        result_description_18 = json_data['prod_18']['description']
        result_description_19 = json_data['prod_19']['description']
        result_description_20 = json_data['prod_20']['description']
        result_description_21 = json_data['prod_21']['description']
        result_description_22 = json_data['prod_22']['description']
        result_description_23 = json_data['prod_23']['description']
        result_description_24 = json_data['prod_24']['description']
        result_description_25 = json_data['prod_25']['description']
        result_description_26 = json_data['prod_26']['description']
        result_description_27 = json_data['prod_27']['description']
        result_description_28 = json_data['prod_28']['description']
        result_description_29 = json_data['prod_29']['description']
        result_description_30 = json_data['prod_30']['description']
        self.assertEqual(expected_description_1, result_description_1)
        self.assertEqual(expected_description_2, result_description_2)
        self.assertEqual(expected_description_3, result_description_3)
        self.assertEqual(expected_description_4, result_description_4)
        self.assertEqual(expected_description_5, result_description_5)
        self.assertEqual(expected_description_6, result_description_6)
        self.assertEqual(expected_description_7, result_description_7)
        self.assertEqual(expected_description_8, result_description_8)
        self.assertEqual(expected_description_9, result_description_9)
        self.assertEqual(expected_description_10, result_description_10)
        self.assertEqual(expected_description_11, result_description_11)
        self.assertEqual(expected_description_12, result_description_12)
        self.assertEqual(expected_description_13, result_description_13)
        self.assertEqual(expected_description_14, result_description_14)
        self.assertEqual(expected_description_15, result_description_15)
        self.assertEqual(expected_description_16, result_description_16)
        self.assertEqual(expected_description_17, result_description_17)
        self.assertEqual(expected_description_18, result_description_18)
        self.assertEqual(expected_description_19, result_description_19)
        self.assertEqual(expected_description_20, result_description_20)
        self.assertEqual(expected_description_21, result_description_21)
        self.assertEqual(expected_description_22, result_description_22)
        self.assertEqual(expected_description_23, result_description_23)
        self.assertEqual(expected_description_24, result_description_24)
        self.assertEqual(expected_description_25, result_description_25)
        self.assertEqual(expected_description_26, result_description_26)
        self.assertEqual(expected_description_27, result_description_27)
        self.assertEqual(expected_description_28, result_description_28)
        self.assertEqual(expected_description_29, result_description_29)
        self.assertEqual(expected_description_30, result_description_30)

    def teste_categoria(self):
        result_category_1 = json_data['prod_1']['category']
        result_category_2 = json_data['prod_2']['category']
        result_category_3 = json_data['prod_3']['category']
        result_category_4 = json_data['prod_4']['category']
        result_category_5 = json_data['prod_5']['category']
        result_category_6 = json_data['prod_6']['category']
        result_category_7 = json_data['prod_7']['category']
        result_category_8 = json_data['prod_8']['category']
        result_category_9 = json_data['prod_9']['category']
        result_category_10 = json_data['prod_10']['category']
        result_category_11 = json_data['prod_11']['category']
        result_category_12 = json_data['prod_12']['category']
        result_category_13 = json_data['prod_13']['category']
        result_category_14 = json_data['prod_14']['category']
        result_category_15 = json_data['prod_15']['category']
        result_category_16 = json_data['prod_16']['category']
        result_category_17 = json_data['prod_17']['category']
        result_category_18 = json_data['prod_18']['category']
        result_category_19 = json_data['prod_19']['category']
        result_category_20 = json_data['prod_20']['category']
        result_category_21 = json_data['prod_21']['category']
        result_category_22 = json_data['prod_22']['category']
        result_category_23 = json_data['prod_23']['category']
        result_category_24 = json_data['prod_24']['category']
        result_category_25 = json_data['prod_25']['category']
        result_category_26 = json_data['prod_26']['category']
        result_category_27 = json_data['prod_27']['category']
        result_category_28 = json_data['prod_28']['category']
        result_category_29 = json_data['prod_29']['category']
        result_category_30 = json_data['prod_30']['category']
        self.assertEqual(expected_category_1, result_category_1)
        self.assertEqual(expected_category_2, result_category_2)
        self.assertEqual(expected_category_3, result_category_3)
        self.assertEqual(expected_category_4, result_category_4)
        self.assertEqual(expected_category_5, result_category_5)
        self.assertEqual(expected_category_6, result_category_6)
        self.assertEqual(expected_category_7, result_category_7)
        self.assertEqual(expected_category_8, result_category_8)
        self.assertEqual(expected_category_9, result_category_9)
        self.assertEqual(expected_category_10, result_category_10)
        self.assertEqual(expected_category_11, result_category_11)
        self.assertEqual(expected_category_12, result_category_12)
        self.assertEqual(expected_category_13, result_category_13)
        self.assertEqual(expected_category_14, result_category_14)
        self.assertEqual(expected_category_15, result_category_15)
        self.assertEqual(expected_category_16, result_category_16)
        self.assertEqual(expected_category_17, result_category_17)
        self.assertEqual(expected_category_18, result_category_18)
        self.assertEqual(expected_category_19, result_category_19)
        self.assertEqual(expected_category_20, result_category_20)
        self.assertEqual(expected_category_21, result_category_21)
        self.assertEqual(expected_category_22, result_category_22)
        self.assertEqual(expected_category_23, result_category_23)
        self.assertEqual(expected_category_24, result_category_24)
        self.assertEqual(expected_category_25, result_category_25)
        self.assertEqual(expected_category_26, result_category_26)
        self.assertEqual(expected_category_27, result_category_27)
        self.assertEqual(expected_category_28, result_category_28)
        self.assertEqual(expected_category_29, result_category_29)
        self.assertEqual(expected_category_30, result_category_30)

    def teste_imagem(self):
        result_image_1 = json_data['prod_1']['image']
        result_image_2 = json_data['prod_2']['image']
        result_image_3 = json_data['prod_3']['image']
        result_image_4 = json_data['prod_4']['image']
        result_image_5 = json_data['prod_5']['image']
        result_image_6 = json_data['prod_6']['image']
        result_image_7 = json_data['prod_7']['image']
        result_image_8 = json_data['prod_8']['image']
        result_image_9 = json_data['prod_9']['image']
        result_image_10 = json_data['prod_10']['image']
        result_image_11 = json_data['prod_11']['image']
        result_image_12 = json_data['prod_12']['image']
        result_image_13 = json_data['prod_13']['image']
        result_image_14 = json_data['prod_14']['image']
        result_image_15 = json_data['prod_15']['image']
        result_image_16 = json_data['prod_16']['image']
        result_image_17 = json_data['prod_17']['image']
        result_image_18 = json_data['prod_18']['image']
        result_image_19 = json_data['prod_19']['image']
        result_image_20 = json_data['prod_20']['image']
        result_image_21 = json_data['prod_21']['image']
        result_image_22 = json_data['prod_22']['image']
        result_image_23 = json_data['prod_23']['image']
        result_image_24 = json_data['prod_24']['image']
        result_image_25 = json_data['prod_25']['image']
        result_image_26 = json_data['prod_26']['image']
        result_image_27 = json_data['prod_27']['image']
        result_image_28 = json_data['prod_28']['image']
        result_image_29 = json_data['prod_29']['image']
        result_image_30 = json_data['prod_30']['image']
        self.assertEqual(expected_image_1, result_image_1)
        self.assertEqual(expected_image_2, result_image_2)
        self.assertEqual(expected_image_3, result_image_3)
        self.assertEqual(expected_image_4, result_image_4)
        self.assertEqual(expected_image_5, result_image_5)
        self.assertEqual(expected_image_6, result_image_6)
        self.assertEqual(expected_image_7, result_image_7)
        self.assertEqual(expected_image_8, result_image_8)
        self.assertEqual(expected_image_9, result_image_9)
        self.assertEqual(expected_image_10, result_image_10)
        self.assertEqual(expected_image_11, result_image_11)
        self.assertEqual(expected_image_12, result_image_12)
        self.assertEqual(expected_image_13, result_image_13)
        self.assertEqual(expected_image_14, result_image_14)
        self.assertEqual(expected_image_15, result_image_15)
        self.assertEqual(expected_image_16, result_image_16)
        self.assertEqual(expected_image_17, result_image_17)
        self.assertEqual(expected_image_18, result_image_18)
        self.assertEqual(expected_image_19, result_image_19)
        self.assertEqual(expected_image_20, result_image_20)
        self.assertEqual(expected_image_21, result_image_21)
        self.assertEqual(expected_image_22, result_image_22)
        self.assertEqual(expected_image_23, result_image_23)
        self.assertEqual(expected_image_24, result_image_24)
        self.assertEqual(expected_image_25, result_image_25)
        self.assertEqual(expected_image_26, result_image_26)
        self.assertEqual(expected_image_27, result_image_27)
        self.assertEqual(expected_image_28, result_image_28)
        self.assertEqual(expected_image_29, result_image_29)
        self.assertEqual(expected_image_30, result_image_30)

    def teste_price(self):
        result_price_1 = json_data['prod_1']['price']
        result_price_2 = json_data['prod_2']['price']
        result_price_3 = json_data['prod_3']['price']
        result_price_4 = json_data['prod_4']['price']
        result_price_5 = json_data['prod_5']['price']
        result_price_6 = json_data['prod_6']['price']
        result_price_7 = json_data['prod_7']['price']
        result_price_8 = json_data['prod_8']['price']
        result_price_9 = json_data['prod_9']['price']
        result_price_10 = json_data['prod_10']['price']
        result_price_11 = json_data['prod_11']['price']
        result_price_12 = json_data['prod_12']['price']
        result_price_13 = json_data['prod_13']['price']
        result_price_14 = json_data['prod_14']['price']
        result_price_15 = json_data['prod_15']['price']
        result_price_16 = json_data['prod_16']['price']
        result_price_17 = json_data['prod_17']['price']
        result_price_18 = json_data['prod_18']['price']
        result_price_19 = json_data['prod_19']['price']
        result_price_20 = json_data['prod_20']['price']
        result_price_21 = json_data['prod_21']['price']
        result_price_22 = json_data['prod_22']['price']
        result_price_23 = json_data['prod_23']['price']
        result_price_24 = json_data['prod_24']['price']
        result_price_25 = json_data['prod_25']['price']
        result_price_26 = json_data['prod_26']['price']
        result_price_27 = json_data['prod_27']['price']
        result_price_28 = json_data['prod_28']['price']
        result_price_29 = json_data['prod_29']['price']
        result_price_30 = json_data['prod_30']['price']
        self.assertEqual(expected_price_1, result_price_1)
        self.assertEqual(expected_price_2, result_price_2)
        self.assertEqual(expected_price_3, result_price_3)
        self.assertEqual(expected_price_4, result_price_4)
        self.assertEqual(expected_price_5, result_price_5)
        self.assertEqual(expected_price_6, result_price_6)
        self.assertEqual(expected_price_7, result_price_7)
        self.assertEqual(expected_price_8, result_price_8)
        self.assertEqual(expected_price_9, result_price_9)
        self.assertEqual(expected_price_10, result_price_10)
        self.assertEqual(expected_price_11, result_price_11)
        self.assertEqual(expected_price_12, result_price_12)
        self.assertEqual(expected_price_13, result_price_13)
        self.assertEqual(expected_price_14, result_price_14)
        self.assertEqual(expected_price_15, result_price_15)
        self.assertEqual(expected_price_16, result_price_16)
        self.assertEqual(expected_price_17, result_price_17)
        self.assertEqual(expected_price_18, result_price_18)
        self.assertEqual(expected_price_19, result_price_19)
        self.assertEqual(expected_price_20, result_price_20)
        self.assertEqual(expected_price_21, result_price_21)
        self.assertEqual(expected_price_22, result_price_22)
        self.assertEqual(expected_price_23, result_price_23)
        self.assertEqual(expected_price_24, result_price_24)
        self.assertEqual(expected_price_25, result_price_25)
        self.assertEqual(expected_price_26, result_price_26)
        self.assertEqual(expected_price_27, result_price_27)
        self.assertEqual(expected_price_28, result_price_28)
        self.assertEqual(expected_price_29, result_price_29)
        self.assertEqual(expected_price_30, result_price_30)

if __name__ == '__main__':
    unittest.main()
